;; -*- lexical-binding: t; -*-

(use-package ox-moderncv
  :vc (ox-moderncv
       :url "https://gitlab.com/Titan-C/org-cv"
       :rev :newest)
  :demand t
  :after org)
